CREATE TABLE COMPANies(
   ID INT PRIMARY KEY     NOT NULL,
   NAME           TEXT    NOT NULL,
   AGE            INT,
   ADDRESS        CHAR(50)
);

insert into companies values(1,'RCCL',52,'5/2 Gaznabi Rd, Dhaka');
insert into companies values(2,'DCL',21,'5/2 Gaznabi Rd, Dhaka');
insert into companies values(3,'AgamiTV',2,'5011 McCormick Mountain Dr, Austin TX');
insert into companies values(4,'Sorted Set',4,'11000 Shallow Water Rd Austin TX');

